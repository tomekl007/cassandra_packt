package com.tomekl007;

import com.datastax.driver.core.Session;
import com.tomekl007.model.TimeSeriesRecord;
import org.junit.Test;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


public class CassandraConnectTest {
    @Test
    public void shouldConnectToLocalCassandraAndFetchAllTimeSeriesData() {
        //given
        CassandraConnect cassandraConnect = new CassandraConnect();
        Session session = cassandraConnect.connect();

        //when
        List<TimeSeriesRecord> result
                = cassandraConnect.selectAllFromTimeSeries(session);
        System.out.println(result);
        //then
        assertThat(result.size()).isGreaterThan(0);
    }

    @Test
    public void shouldInsertNewEventUsingPreparedStatement() {
        //given
        CassandraConnect cassandraConnect = new CassandraConnect();
        Session session = cassandraConnect.connect();
        TimeSeriesRecord timeSeriesRecord
                = new TimeSeriesRecord("id", new Date(), "unique_content_for_this_event");

        //when
        cassandraConnect.insertNewEvent(session, timeSeriesRecord);

        //then
        List<TimeSeriesRecord> timeSeriesRecords = cassandraConnect.selectAllFromTimeSeries(session);


        assertThat(hasInsertedTimeSeriesRecord(timeSeriesRecords, timeSeriesRecord));
    }

    private boolean hasInsertedTimeSeriesRecord(List<TimeSeriesRecord> timeSeriesRecords,
                                                TimeSeriesRecord expected) {
        return timeSeriesRecords
                .stream()
                .anyMatch(
                        v -> v.getContent().equals(expected.getContent()) &&
                                v.getId().equals(expected.getId())
                );
    }


}