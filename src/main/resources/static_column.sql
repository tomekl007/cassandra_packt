CREATE TABLE t(
  k text,
  s text static,
  i int,
  PRIMARY KEY(k,i)
);
INSERT INTO t (k, s, i) VALUES ('k', 'shared', 0)
INSERT INTO t (k, s, i) VALUES ('k', 'still shared', 1)
SELECT * FROM t;

#return
k | s              | i
k | 'still shared' | o